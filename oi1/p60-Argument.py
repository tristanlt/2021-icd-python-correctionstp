import sys

# Test si argument prod/dev est passé
# Argument dans mode 
if len(sys.argv) < 2:
    print('Merci de donner un argument (prod/dev)')
    sys.exit(1)

# Test si l'argument est prod ou dev
# Sinon retourne a l'utilisateur un conseil et sort
if sys.argv[1] not in ["prod", "dev"]:
    print('Merci de donner un argument (prod/dev)')
    sys.exit(1)

# On place l'argument validé par les tests plus dans la variable mode
mode = sys.argv[1]

### Fonction de sélection des paramètres
def serverFilter(srv, ip, state):
    """
    Filtre les serveurs qui doivent être redémarrés
    Si "10.0.42.", si éteind, si dans mode en params
    Input: srv (str), ip (str), state(boolean)
    Ouput: boolean
    """
    if (state == True) \
        or (not "10.0.42." in ip) \
        or (F"-{mode}-" not in srv):
        return False
    else:
        return True

srv = ['s000-prod-1', 'n022-dev-1', 's054-prod-4', 's012-dev-1', 's001-prod-1']
ip = ['10.0.42.1', '10.0.42.4', '10.0.18.200', '10.0.43.8', '10.0.42.48' ]
state = [ True, False, False, False, False]

for i in range( len(srv)):
    testSrv = serverFilter(srv[i], ip[i], state[i])
    if testSrv:
        print(F"{srv[i]},{ip[i]},{state[i]}")