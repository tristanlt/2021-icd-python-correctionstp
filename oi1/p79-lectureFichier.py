

allServers = []
filename = "./extract-srv-appli1.txt"


dataFile = open(filename, "r")
for l in dataFile:
    #l(str) -> "nomserver,10.0.42.12,up\n"
    #l[1] 
    srvInfos = l[:-1].split(",")
    # ['nomserver', '10.0.42.12', 'up']
    # Transformation de up/down en boolean
    if srvInfos[2] == "up":
        srvState = True
    else:
        srvState = False
    # Création d'un dict temporaire
    server = {"srv": srvInfos[0],
              "ip": srvInfos[1],
              "state": srvState }
    # Ajout allServer
    allServers.append(server)
dataFile.close()

print(F"{len(allServers)} trouvés dans le fichier")
print(allServers[1])