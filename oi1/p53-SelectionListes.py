srv = ['s000-prod-1', 'n022-prod-1', 's054-prod-4', 's012-dev-1', 's001-prod-1']
ip = ['10.0.42.1', '10.0.42.4', '10.0.18.200', '10.0.43.8', '10.0.42.48' ]
state = [ True, False, False, False, False]


for i in range(len(srv)):
    if state[i] == True:
        continue
    if not "10.0.42." in ip[i]:
        continue
    if "-prod-" not in srv[i]:
        continue
    print(F"{srv[i]},{ip[i]}")
