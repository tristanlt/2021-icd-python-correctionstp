
# Calcul de moyenne For
cumul = 0
notes = [14, 9, 6, 8, 12]
for note in notes:
    cumul = cumul + note
nbNotes = len(notes)
print(F"La moyenne est {cumul / nbNotes}")


# Dessiner un demi-sapin (while)
i = 1
while i <= 10:
    print("*" * i)
    i = i + 1
