

# Demande de l'age.
nom = input('Quel est ton nom ? :')
# Affectation d'un age qui ne remplit pas les conditions de la boucle
age = -1

while not ((age > 0) and (age < 150)):
    reponse = input('Quel est ton age ? :')
    age = int(reponse)

print(f"{nom}, tu as {age} ans!")
