def serverFilter(srv, ip, state):
    """
    Filtre les serveurs qui doivent être redémarrés
    Si "10.0.42.", si éteind, si dans mode en params
    Input: srv (str), ip (str), state(boolean)
    Ouput: boolean
    """
    if (state == True) \
        or (not "10.0.42." in ip) \
        or (F"-prod-" not in srv):
        return False
    else:
        return True


srv = ['s000-prod-1', 'n022-prod-1', 's054-prod-4', 's012-dev-1', 's001-prod-1']
ip = ['10.0.42.1', '10.0.42.4', '10.0.18.200', '10.0.43.8', '10.0.42.48' ]
state = [ True, False, False, False, False]

allServers = []

for i in range(len(srv)):
    # Pour chaque serveur, on met les informations dans un dictionnaires
    server = {"srv": srv[i],
              "ip": ip[i],
              "state": state[i]}
    # Puis on ajoute ce dict a une liste
    allServers.append(server)

# Itération sur la liste
for s in allServers:
    # On peut utiliser les clefs du dict
    testSrv = serverFilter(s['srv'], s['ip'], s['state'])
    if testSrv:
        print(F"{s['srv']},{s['ip']}")

# Itération sur la liste avec **s
# **s va transformer les "clefs/value" en "paramètre/value" de la fonction
for s in allServers:
    # On peut utiliser les clefs du dict
    testSrv = serverFilter(**s)
    if testSrv:
        print(F"{s['srv']},{s['ip']}")