import sys

### Fonction de sélection des paramètres
def serverFilter(srv, ip, state):
    """
    Filtre les serveurs qui doivent être redémarrés
    Si "10.0.42.", si éteind, si dans mode en params
    Input: srv (str), ip (str), state(boolean)
    Ouput: boolean
    """
    if (state == True) \
        or (not "10.0.42." in ip) \
        or (F"-prod-" not in srv):
        return False
    else:
        return True

srv = ['s000-prod-1', 'n022-dev-1', 's054-prod-4', 's012-dev-1', 's001-prod-1']
ip = ['10.0.42.1', '10.0.42.4', '10.0.18.200', '10.0.43.8', '10.0.42.48' ]
state = [ True, False, False, False, False]

for i in range( len(srv)):

    # utilisation de fonction pour savoir si le serveur est concerné ou pas    
    testSrv = serverFilter(srv[i], ip[i], state[i])
    # testSrv est un booleen (true/false)
    if testSrv:
        print(F"{srv[i]},{ip[i]}")